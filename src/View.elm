module View exposing (viewStyled)

import Css exposing (..)
import Html.Styled exposing (..)
import Html.Styled.Attributes as A
import Html.Styled.Events exposing (on, preventDefaultOn)
import Json.Decode as D
import Model exposing (..)


viewStyled : Model -> Html Msg
viewStyled model =
    div [ A.css [ padding (px 10) ] ]
        [ div
            [ A.css
                [ height (px 100)
                , backgroundColor (rgba 200 200 250 1.0)
                , displayFlex
                , justifyContent center
                , alignItems center
                ]
            , on "mousemove" (mousePosDecoder |> D.map Move)
            , A.id "container"
            ]
            [ div [ A.css [pointerEvents none] ] [ text (model.position |> maybePositionToString) ] ]
        , label [ preventDefaultOn "click" (D.succeed (ToggleGranularity, True)) ]
            [ input [ A.type_ "checkbox", A.checked model.snap10 ] []
            , text "Granularity: 10 min"
            ]
        ]


mousePosDecoder : D.Decoder Float
mousePosDecoder =
    D.field "clientX" D.float
        |> D.andThen
            (\clientX ->
                D.at [ "target", "offsetLeft" ] D.float
                    |> D.map (\offsetLeft -> clientX - offsetLeft)
            )
