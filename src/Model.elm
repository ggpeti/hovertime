module Model exposing (..)

import Time exposing (Posix, Zone)
import Time.Extra exposing (Interval(..))


type alias Model =
    { position : Maybe ( Zone, Posix )
    , containerWidth : Maybe Float
    , hereAndNow : Maybe ( Zone, Posix )
    , snap10 : Bool
    }


initModel : Model
initModel =
    { position = Nothing
    , containerWidth = Nothing
    , hereAndNow = Nothing
    , snap10 = False
    }


type Msg
    = Move Float
    | WindowResize
    | ContainerWidthChange (Maybe Float)
    | SetHereAndNow (Maybe ( Zone, Posix ))
    | ToggleGranularity


minutesInDay : Float
minutesInDay =
    1440


calculatePosix : Bool -> Float -> Float -> ( Zone, Posix ) -> ( Zone, Posix )
calculatePosix snap10 x width ( here, now ) =
    let
        factor =
            if snap10 then
                10

            else
                1

        todayStart =
            Time.Extra.floor Day here now

        extraMinutes =
            round (x / width * (minutesInDay - 1) / factor) * factor
    in
    ( here, Time.Extra.add Minute extraMinutes here todayStart )


maybePositionToString : Maybe ( Zone, Posix ) -> String
maybePositionToString maybePosition =
    let
        formatTimeComponent =
            String.padLeft 2 '0' << String.fromInt
    in
    case maybePosition of
        Just ( zone, time ) ->
            formatTimeComponent (Time.toHour zone time) ++ ":" ++ formatTimeComponent (Time.toMinute zone time)

        Nothing ->
            ""
