module Main exposing (..)

import Browser
import Browser.Dom exposing (getViewportOf)
import Browser.Events exposing (onResize)
import Html.Styled exposing (toUnstyled)
import Model exposing (..)
import Result
import Task
import Time
import View exposing (viewStyled)


main : Program () Model Msg
main =
    Browser.element
        { init = always ( initModel, Cmd.batch [ refreshContainerWidth, getHereAndNow ] )
        , update = update
        , subscriptions = always (onResize (always (always WindowResize)))
        , view = viewStyled >> toUnstyled
        }


getHereAndNow : Cmd Msg
getHereAndNow =
    Task.attempt
        (Result.toMaybe >> SetHereAndNow)
        (Time.here |> Task.andThen (\here -> Time.now |> Task.map (\now -> ( here, now ))))


refreshContainerWidth : Cmd Msg
refreshContainerWidth =
    Task.attempt
        (Result.toMaybe >> Maybe.map (.viewport >> .width) >> ContainerWidthChange)
        (getViewportOf "container")


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Move x ->
            ( { model | position = model.hereAndNow |> Maybe.map2 (calculatePosix model.snap10 x) model.containerWidth }, Cmd.none )

        WindowResize ->
            ( model, refreshContainerWidth )

        ContainerWidthChange maybeWidth ->
            ( { model | containerWidth = maybeWidth }, Cmd.none )

        SetHereAndNow maybeHereAndNow ->
            ( { model | hereAndNow = maybeHereAndNow }, Cmd.none )

        ToggleGranularity ->
            ( { model | snap10 = not model.snap10 }, Cmd.none )
